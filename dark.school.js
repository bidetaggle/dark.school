const express = require('express');
const app = express();
const port = 3000;

app.set('view engine', 'pug');
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => res.render('index'));

app.get('/txt', (req, res) => res.render('txt'));

app.get('/videos', (req, res) => res.render('videos', { Videos: [
    {
        title: 'Peace - Antonopoulos',
        url: 'https://www.youtube-nocookie.com/embed/2HBmqn1F91A'
    },
    {
        title: 'Inner world, outer world - Part 1 - Akasha',
        url: 'https://www.youtube-nocookie.com/embed/aXuTt7c3Jkg'
    },
    {
        title: 'Nothing to hide (VOSTFR)',
        url: 'https://peertube.cpy.re/videos/embed/d2a5ec78-5f85-4090-8ec5-dc1102e022ea'
    },
    {
        title: 'Miles vs. Watson: The Complete Man Against Machine Showdown',
        url: 'https://www.youtube.com/watch?v=YgYSv2KSyWg&ab_channel=PBSNewsHour'
    }
]}));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
