$(document).ready(function(){
    $("li").click(function(e){

        $("div#player .frame").html(`<iframe width='560' height='315' src='${e.target.dataset.link}' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen=''></iframe>`);
        $("div#player .title").html(`${e.target.dataset.link}`);
        $("div#player").fadeIn();
    });
    $(".closeButton").click(function(e){
        $("div#player").fadeOut(400,() => {
            $("div#player .frame").html('');
        });
    });
});
